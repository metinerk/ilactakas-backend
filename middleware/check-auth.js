const jwt = require('jsonwebtoken')
const privateKey = require('../privateKey')

module.exports = (req, res, next) => {
    
    try{
        const token = req.headers.authorization.split(" ")[1]
        jwt.verify(token, privateKey)
        next()
    }
    catch( err ) {
        res.status(401).json({
            message: "Auth failed!"
        })
    }
}