const express = require('express')
const router = express.Router()
const Bid = require("../models/bid")

// middleware that checks user is authenticated via looking its jwt.
const checkAut = require('../middleware/check-auth')

router.post("/createBid",checkAut,(req, res, next) => {

    const bid = new Bid(req.body)
    bid.save()
      .then( () => {
        res.status(201).json({
          message: 'Bid created succesfully',
          bid: bid
        })
      })
  });
  
  router.get("/getBids",checkAut,(req,res) => {
    Bid.find()
      .then( documents => {
        res.status(201).json({
          message: 'Bid informations are successfully getted.',
          bids: documents
        })
      })
  })
  
  module.exports = router 