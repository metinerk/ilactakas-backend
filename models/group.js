const mongoose = require('mongoose')
const groupSchema = mongoose.Schema({
    name: String
})

module.exports = mongoose.model('Group', groupSchema)