const mongoose = require('mongoose')

//throws a error if existing email will try to signup. Otherwise unique tag is useless.
const uniqueValidator = require('mongoose-unique-validator')

const userSchema = mongoose.Schema({
    
    name: { type:String, require: true},
    gln: { type:String, require: true},
    email: { type:String, require: true, unique: true},
    password: { type:String, require: true},
    group_id: { type:String, require: true}

})

userSchema.plugin(uniqueValidator)

module.exports = mongoose.model('User', userSchema)