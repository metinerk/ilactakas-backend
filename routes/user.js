const express = require('express')
const route = express.Router()
const User = require('../models/user')
const  privateKey = require('../privateKey')

// the plugin that helps creation of jwt token and its functions.
const jwt = require('jsonwebtoken')

//the pluging to crypt password to more secure application.
const bcrypt = require('bcrypt')

route.post('/signup', (req, res, next) => {
    
    bcrypt.hash(req.body.password, 10)
        .then( result => {
            req.body.password = result 
            let user = new User(req.body)
            user.save()
                .then( result => {
                    res.status(201).json({
                        message: "Successfully signed up!",
                    })
                })
                .catch( error => {
                    res.status(500).json({
                        message: 'E-MAİL ZATEN MEVCUT'
                    })        
                })
            
        })
        .catch( error => {
            res.status(500).json({
                message: error
            })
        })

})

route.post('/login', (req,res,next)=> {
    let fetchedUser;
    User.findOne( {email: req.body.email} )
        .then( user =>{
            if(!user){
                return res.status(401).json({
                    message: "couldnt find e-mail!"
                })
            }
            else{
                fetchedUser = user
                return bcrypt.compare(req.body.password,fetchedUser.password)
            }
        })
        .then( result => {
            if(!result){
                return res.status(401).json({
                    message: "passwords doesnt match!" 
                })
            }
            fetchedUser.password = ""
            const jwtToken = jwt.sign({
                user: fetchedUser
            }, privateKey, {expiresIn: '1hr'})
            res.status(200).json({
                token: jwtToken,
                user: fetchedUser
            })
        })
        .catch( err => {
            return res.status(401).json({
                message: "autho failed!" 
            })
        }) 
})

module.exports = route
