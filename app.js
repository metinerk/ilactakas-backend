const express = require("express");
const mongoose = require("mongoose")

const app = express();
const bodyParser = require('body-parser')

const bidRouter = require("./routes/bids")
const userRouter = require("./routes/user")

const ilaclar = require("./ilaclar")
const Group = require("./models/group")



//connection of mondodb atlas
mongoose.connect("mongodb+srv://metiner:q1w2e3RRrmetiner@ilactakas-q2ufq.mongodb.net/ilactakas")
  .then( () => {
    console.log("Database successfully connected!")
  })
  .catch( () => {
    console.log("Couldnt connect to the database!")
  })

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS"
  );
  next();
});


// autocomplete for ilac search
app.post("/getIlac", (req,res) => {
          
  if(req.body.searchParam.length > 3){
      let ilaclars= ilaclar.ilaclar.filter(element => {
        return element['ILAC ADI'].includes(req.body.searchParam)
      })
      res.status(201).json({
          ilaclar: ilaclars
      }
    )}
})

//returns ecza groups
app.get('/getGroups',(req, res) => {
    Group.find()
      .then( documents => {
        res.status(201).json({
          groups: documents
        })
      })
})

app.use('/bids/', bidRouter)
app.use('/user/', userRouter)

module.exports = app;
