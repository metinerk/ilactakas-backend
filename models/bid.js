const mongoose = require('mongoose')
const bidSchema = mongoose.Schema({
    barcodeNo: { type: String, require: true},
    alimMiktari: { type: Number, require: true},
    malFazlasi: { type: Number, require: true},
    minAlim: { type: Number, require: true},
    maxAlim: { type: Number, require: true},
    talepBasTar: { type: String, require: true},
    talepBitTar: { type: String, require: true},
    hedeflenenAlim: { type: Number, require: true},
    sonKulTar: { type: String, require: true},
    etiketFiy: { type: Number, require: true},
    depoFiy: { type: Number, require: true},
    hedefAlimGecilsin: Boolean,
    eczaneyeOzel: Boolean,
    eczane: String,
    createdBy: { type: String, require: true},
    netFiyat: { type: Number, require: true},
    alinan: { type: Number, require: true}
})
module.exports = mongoose.model("Bid",bidSchema);